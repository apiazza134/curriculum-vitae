![build status](https://gitlab.com/apiazza134/curriculum-vitae/badges/master/pipeline.svg)

# Curriculum Vitae

The most recent PDF is available [here](https://gitlab.com/apiazza134/curriculum-vitae/-/jobs/artifacts/master/raw/cv.pdf?job=compile_pdf).
